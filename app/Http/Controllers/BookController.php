<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Resources\BooksResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;


class BookController extends Controller
{
    //
    public function index(){

        $book = Book::with('ratings')->orderBy('created_at','desc')->paginate(5);
      //  $bookjsonSerialize(JSON_PRETTY_PRINT);
        return BooksResource::collection($book);
    }

    public function delete_book($id){

        $book = Book::destroy($id);
        return response()->json($book);
    }

    public function store_book(Request $request){

        $book = new Book();

        $book->title = $request->input('title');
        $book->description = $request->input('description');

        if($book->save()){
            return response()->json($book);
        }

    }

    public function edit_book(Request $request){

        $book = Book::find($request->article_id);

        $book->title = $request->title;
        $book->description = $request->description;

        if($book->update()){
            return response()->json($book);
        }
        
    }
}
